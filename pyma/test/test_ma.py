from unittest import TestCase

import pyma

class TestSimpleMA(TestCase):

    def test_ma(self):
        SimpleMA = pyma.SimpleMA(5)
        results = [[2,2],[2.5,3],[3.333333,5],[3.25,3],[3.2,3],[4.2,7],[4.10928,2.5464],[4.90928,9],[4.30928,0],[3.10928,-3],[0.57368,-5.678],[0.0644,0],[-1.7356,0],[-0.1356,8],[0.4644,0],[1.6,0]]
        for result in results:
            self.assertEqual(result[0], round(SimpleMA.compute(result[1]), 6))

    def test_ma_empty_parameter(self):
        #brute testing some valid value, rounded to 5 decimal
        SimpleMA = pyma.SimpleMA(5, "-")
        self.assertEqual("-",         SimpleMA.compute(2))
        self.assertEqual("-",         SimpleMA.compute(3))
        self.assertEqual("-",         SimpleMA.compute(5))
        self.assertEqual("-",         SimpleMA.compute(3))
        self.assertEqual(3.2,       round(SimpleMA.compute(3), 5))
        self.assertEqual(4.2,       round(SimpleMA.compute(7), 5))
        self.assertEqual(4.10928,   round(SimpleMA.compute(2.5464), 5))
        self.assertEqual(4.90928,   round(SimpleMA.compute(9), 5))
        self.assertEqual(4.30928,   round(SimpleMA.compute(0), 5))
        self.assertEqual(3.10928,   round(SimpleMA.compute(-3), 5))
        self.assertEqual(0.57368,   round(SimpleMA.compute(-5.678), 5))
        self.assertEqual(0.0644,    round(SimpleMA.compute(0), 5))
        self.assertEqual(-1.7356,   round(SimpleMA.compute(0), 5))
        self.assertEqual(-0.1356,   round(SimpleMA.compute(8), 5))
        self.assertEqual(0.4644,    round(SimpleMA.compute(0), 5))
        self.assertEqual(1.6,       round(SimpleMA.compute(0), 5))

    def test_EMA(self):
        EMA = pyma.EMA(0.2)
        results = [29.341320155,29.34132],[44.779564776,32.42897],[51.659712089,36.27512],[50.7477490173,39.16964],[57.6729369253,42.8703],[60.3947035553,46.37518],[66.2854336684,50.35723],[69.3876589566,54.16332],[47.6994037412,52.87054],[35.4885412132,49.39414]
        for result in results:
            self.assertEqual(result[1], round(EMA.compute(result[0]), 5))

    def test_NDayEma(self):
        n = 20
        NDayEMA = pyma.NDayEMA(n)
        EMA = pyma.EMA(2.0000/(n))
        self.assertEqual(NDayEMA.compute(33), EMA.compute(33))

    def test_CMA(self):
        CMA = pyma.CMA()
        self.assertEqual(1,     CMA.compute(1))
        self.assertEqual(1.5,   CMA.compute(2))
        self.assertEqual(8,     CMA.compute(21))
        self.assertEqual(8.5,   CMA.compute(10))
        self.assertEqual(13,    CMA.compute(31))
        self.assertEqual(15.5,  CMA.compute(28))
        self.assertEqual(14,    CMA.compute(5))
        self.assertEqual(12.5,  CMA.compute(2))

    def test_WMA(self):
        WMA = pyma.WMA(5)
        self.assertEqual(1,     WMA.compute(1))
        self.assertEqual(7,     WMA.compute(10))
        self.assertEqual(5.5,   WMA.compute(4))
        self.assertEqual(7.3,   WMA.compute(10))
        self.assertEqual(15.2,  WMA.compute(31))
        self.assertEqual(20.8,  WMA.compute(28))
        self.assertEqual(16.6,  WMA.compute(4))
        self.assertEqual(13.8,  WMA.compute(7))

    def test_WMA(self):
        WMA = pyma.WMA(5, "-")
        self.assertEqual("-",   WMA.compute(1))
        self.assertEqual("-",   WMA.compute(10))
        self.assertEqual("-",   WMA.compute(4))
        self.assertEqual("-",   WMA.compute(10))
        self.assertEqual(15.2,  WMA.compute(31))
        self.assertEqual(20.8,  WMA.compute(28))
        self.assertEqual(16.6,  WMA.compute(4))
        self.assertEqual(13.8,  WMA.compute(7))